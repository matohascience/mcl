#!/usr/bin/env python3

# Script to connect to the device over USB and display the logs (terminal output)
#
# This is useful to debug the device behaviour.
#
# Usage: python3 connect_to_device.py
# To exit, press Ctrl+C

from serial.tools import list_ports
import serial


# Try to detect the device's USB port
port_list = list_ports.grep(r"ttyUSB|usbserial|usbmodem", include_links=False)

# The port_list a generator object so convert it to a list
port_list = [port for port in port_list]

if not port_list:
    raise RuntimeError("Device not detected")

print(f"Connecting to the first detected device ({port_list[0].device}). Ctrl + C to exit.")
with serial.Serial(port_list[0].device, 115200, timeout=1) as ser:

    # Reset the device
    ser.setRTS(True)
    ser.setDTR(False)
    ser.setRTS(False)

    # Read the serial port and print the output until terminated by Ctrl+C
    while 1:
        try:
            line = ser.readline().decode("utf-8").strip()
            if line:
                print(line)
        except UnicodeDecodeError:
            pass
