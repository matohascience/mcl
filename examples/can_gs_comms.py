# *Communications example using a GS-CAN adapter*
#
# Warning - as we had stability issues with GSCAN on Mac, we have internally switched to python-can. If GS-CAN works for
# you - feel free to continue using it
#
# This example assumes a GS-CAN (Geschwister Schneider, candleLight) USB to CAN adapter is connected to your
# computer.
#
# The driver used is https://pypi.org/project/gs-usb/ , available via pip under the MIT License
#
# (c) 2021 Matoha Instrumentation Ltd, Licensed under the MIT license


from gs_usb.gs_usb import GsUsb
from gs_usb.gs_usb_frame import GsUsbFrame
import isotp
import time

import sys
import os
sys.path.append(os.path.realpath('.'))

from mcl.matoha_can import MatohaCAN


# Adjust this to your configuration, numbers below are Matoha default
can_bitrate = 250000
matoha_can_id = 0x400
matoha_can_tx_offset = 1
matoha_can_rx_offset = 0

can_adapter = None

# Set this to true to print all the frames coming in/out
print_traffic = False

# Set this to true to print device logs as they come in
print_logs = True


def setup_function():
    global can_adapter
    # Scan across USB devices to find our CAN adapter
    can_adapters = GsUsb.scan()
    if len(can_adapters) == 0:
        print("Couldn't find any gs_usb devices")

    # Assume there is only one CAN-USB adapter - modify this as needed
    can_adapter = can_adapters[0]

    # Start comms
    can_adapter.start()

    if not can_adapter.set_bitrate(can_bitrate):
        print("Can not set bitrate for gs_usb")


def rx_function():
    """
    Custom receive function to receive data over a GS CAN adapter from a Matoha device

    :return: isotp.CanMessage object with the received frame
    """
    global can_adapter
    frame = GsUsbFrame()

    if can_adapter.read(frame, 1):
        if frame.can_id != matoha_can_id+matoha_can_rx_offset and frame.can_id != matoha_can_id+matoha_can_tx_offset:
            # Not our traffic, ignore
            print(f"RX  {frame} --- drop")
            return

        if print_traffic:
            print(f"RX  {frame}")

        return isotp.CanMessage(arbitration_id=frame.can_id, data=bytearray(frame.data), dlc=frame.can_dlc,
                                extended_id=frame.is_extended_id)


def tx_function(msg):
    """
    Custom receive function to transmit data over a GS CAN adapter to a Matoha device

    :param msg: ISOTP message object
    :return: None
    """
    global can_adapter
    frame = GsUsbFrame(can_id=msg.arbitration_id, data=list(msg.data))
    frame.can_dlc = msg.dlc
    can_adapter.send(frame)

    if print_traffic:
        print(f"TX  {frame}")


def shutdown_function():
    """
    Custom shutdown function to gracefully terminate our GS CAN adapter comms.
    :return: None
    """
    global can_adapter
    can_adapter.stop()


if __name__ == '__main__':

    matoha_dev = MatohaCAN(can_tx_address=matoha_can_id + matoha_can_tx_offset,
                    can_rx_address=matoha_can_id + matoha_can_rx_offset,
                    setup_function=setup_function,
                    shutdown_function=shutdown_function,
                    rx_function=rx_function,
                    tx_function=tx_function
                    )
    matoha_dev.start_comms()

    start = time.time()

    # The functions below will be called in periodic intervals
    things_to_do = [
        matoha_dev.start_measuring, matoha_dev.stop_measuring, matoha_dev.single_measurement,
        matoha_dev.request_status,
                    matoha_dev.sleep,

                    matoha_dev.wake_up,
                    matoha_dev.request_status
    ]

    while 1:
        time.sleep(0.1)
        if matoha_dev.available():
            msg = matoha_dev.receive()
            if "log" in msg:
                if print_logs:
                    print(f"Device log: {msg['log']}", end="")
            else:
                print(msg)

        # Optionally terminate when nothing else to do
        #if len(things_to_do) == 0:
        #    break

        # Every 10 seconds send a command
        if (time.time()-start) > 10 and len(things_to_do) > 0:
            thing_to_do = things_to_do.pop(0)
            print(f"Executing: {thing_to_do}")
            thing_to_do()
            start = time.time()


    matoha_dev.terminate_comms()
