# *Communications example using the python-can library*
#
# This example should support any python-can adapter.
#
# The driver used is https://python-can.readthedocs.io/en/master/ , available via pip under the LGPL License
#
# (c) 2021 Matoha Instrumentation Ltd, Licensed under the MIT license

import can
import isotp
import time

import sys
import os
sys.path.append(os.path.realpath('.'))

from mcl.matoha_can import MatohaCAN


# Adjust this to your configuration, numbers below are Matoha default
can_bitrate = 250000
matoha_can_id = 0x400
matoha_can_tx_offset = 1
matoha_can_rx_offset = 0

# Adjust specific to your CAN adapter
# The settings below are for USBTin, which we use internally - but other adapters will, of course, be supported.
# For bustype and chanel values see https://python-can.readthedocs.io/en/master/interfaces.html
bustype = "slcan"
channel = "/dev/tty.usbmodemA02103A41"
can_adapter = None

# Set this to true to print all the frames coming in/out
print_traffic = False

# Should a timestamp be included with each packet?
timestamp_traffic = False
traffic_start = time.time()

# Set this to true to print device logs as they come in
print_logs = True

# Set this to true to nicely format incoming measurements
format_measurements = True

# Plot the incoming spectra - you will need to install numpy and matplotlib!
plot_spectra = False

if plot_spectra:
    import numpy as np
    import matplotlib.pyplot as plt


def setup_function():
    """
    Function to call when setting up the bus

    :return: None
    """
    global can_adapter
    can_adapter = can.Bus(channel=channel, bustype=bustype, bitrate=can_bitrate)


def rx_function():
    """
    Custom receive function to receive data over a GS CAN adapter from a Matoha device

    :return: isotp.CanMessage object with the received frame
    """
    global can_adapter
    frame = can_adapter.recv(0.001)
    if frame:
        if frame.arbitration_id != matoha_can_id+matoha_can_rx_offset:
            # Not our traffic, ignore
            #print(f"RX  {frame} --- drop")
            return
        elif print_traffic:
            if timestamp_traffic:
                print(f"{time.time()-traffic_start:4f} RX  {frame}")
            else:
                print(f"RX  {frame}")

        return isotp.CanMessage(arbitration_id=frame.arbitration_id, data=frame.data, extended_id=frame.is_extended_id,
                                is_fd=frame.is_fd, dlc=frame.dlc)


def tx_function(msg):
    """
    Custom receive function to transmit data over a GS CAN adapter to a Matoha device

    :param msg: ISOTP message object
    :return: None
    """
    global can_adapter
    msg_obj = can.Message(data=msg.data, arbitration_id=msg.arbitration_id, is_extended_id=False)
    can_adapter.send(msg_obj)
    if print_traffic:
        if timestamp_traffic:
            print(f"{time.time()-traffic_start:4f} TX  {msg_obj}")
        else:
            print(f"TX  {msg_obj}")


def shutdown_function():
    """
    Custom shutdown function to gracefully terminate our GS CAN adapter comms.
    :return: None
    """
    global can_adapter
    can_adapter.shutdown()


if __name__ == '__main__':

    matoha_dev = MatohaCAN(can_tx_address=matoha_can_id + matoha_can_tx_offset,
                    can_rx_address=matoha_can_id + matoha_can_rx_offset,
                    setup_function=setup_function,
                    shutdown_function=shutdown_function,
                    rx_function=rx_function,
                    tx_function=tx_function
                    )
    matoha_dev.start_comms()

    start = time.time()

    # The functions below will be called in periodic intervals
    things_to_do = [
        #matoha_dev.start_measuring,
        matoha_dev.request_status,
        matoha_dev.stop_measuring,
        matoha_dev.single_measurement,
        matoha_dev.request_status,
        #matoha_dev.sleep,
        #matoha_dev.request_status,
        #matoha_dev.wake_up,
        #matoha_dev.request_status
        #matoha_dev.run_ota,
        #matoha_dev.restart
    ]

    request_time = time.time()

    # Set to True if you don't want to run measurements in an infinite loop
    measurement_requested = False

    while 1:
        time.sleep(0.01)
        if matoha_dev.available():
            msg = matoha_dev.receive()
            if "log" in msg:
                if print_logs:
                    print(f"Device log: {msg['log']}", end="")
            elif "materialID" in msg and format_measurements:
                print(f"\n\n\n*Measurement received*")
                if msg['metric'] < 0:
                    print(f"An error has occurred: {msg['materialName']}")
                elif msg['signalIntensity'] > 0.95:
                    print("No sample present")
                elif msg['signalIntensity'] < 0.03:
                    print("Non-transparent sample.")
                else:
                    print(f"Material: {msg['materialName']}")
                    print(f"Certainty: {msg['metric']:.2f}")
                    print(f"Measurement took: {time.time()-request_time:3f} sec")
                    print(f"Signal intensity: {int(100*msg['signalIntensity'])}%")
                #print(msg)
                measurement_requested = False
                if plot_spectra and "spectrum" in msg:
                    plt.plot(np.linspace(msg['wavelengthMin'], msg['wavelengthMax'], msg['spectrumPoints']), msg['spectrum'])
                    plt.xlabel("Wavelength (nm)")
                    plt.ylabel("Intensity (arbitrary units)")
                    plt.title("Click X in the top bar to exit and load another")
                    plt.show()
            elif "state" in msg:
                print(f"Device state: {msg}")
            else:
                print(msg)

        # Every 1 second send a command
        if (time.time()-start) > 1:
            if len(things_to_do) > 0:
                thing_to_do = things_to_do.pop(0)
                print(f"Executing: {thing_to_do}")
                thing_to_do()
                start = time.time()
            elif not measurement_requested:  # no more things to do, keep requesting measurements
                time.sleep(0.1)
                matoha_dev.single_measurement()
                print("\n\nTriggering a new measurement!")
                request_time = time.time()
                measurement_requested = True
                start = time.time()
            else:
                matoha_dev.request_status()
                start = time.time()


    matoha_dev.terminate_comms()
