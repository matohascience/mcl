# *Communications example using the pyserial library*
#
# This example demonstrates how to communicate with the device over the Serial/UART/RS232 protocol.
# Please note this functionality is not enabled on the devices by default - contact us for further information.
#
# The driver used is https://pyserial.readthedocs.io/en/latest/ , available via pip under the BSD License
#
# (c) 2023 Matoha Instrumentation Ltd, Licensed under the MIT license

import time
import sys
import os
from serial.tools import list_ports
sys.path.append(os.path.realpath('.'))

from mcl.matoha_serial import MatohaSerial

# Set this to true to print device logs as they come in
print_logs = False

# Set this to true to nicely format incoming measurements
format_measurements = True

# Set this to true to request measurements from the device, otherwise it will just use the automatic trigger
request_measurements = False

port_list = list_ports.grep(r"ttyUSB|usbmodem|usbserial|COM", include_links=False)
print("Available Serial USB ports:")
serial_port = None

for port in port_list:
    if "ttyUSB" not in port.device and "usbserial" not in port.device and "usbmodem" not in port.device and "COM" not in port.device:
        continue
    serial_port = port.device
    print(serial_port)

print(f"Choosing the first suitable port in the list ({serial_port}). (You can override this by editing the script)")
# or override the port name
#serial_port = "/dev/tty.usbserial-140"

if not serial_port:
    raise RuntimeError("No serial ports detected")


matoha_dev = MatohaSerial(serial_port, print_logs=print_logs)
matoha_dev.start_comms()

print("Will read the status every 1s until the device is ready to measure...")

while True:
    status = matoha_dev.get_status()
    if status and status["state"] in ["WAITING_FOR_TRIGGER", "IDLE"]:
            print("The device is ready to measure.")
            print(f"The status is: \n {status}")

            # Typical response (status):
            # {'serialNo': '244cabe7ffff', 'wifiSSID': 'MyWifiSSID', 'ipAddress': '192.168.1.250',
            # 'firmwareVersion': 'b60989a', 'sensorCount': 1, 'state': 'IDLE', 'protocolVersion': 11}

            break
    time.sleep(1)

if request_measurements:
    # Stop the machine from measuring automatically (in case the autotrigger was enabled)
    matoha_dev.stop_measuring()
    time.sleep(1)

start_time = time.time()
measurement_requested = False
request_time = 0

while 1:
    time.sleep(0.01)
    if matoha_dev.available():
        msg = matoha_dev.receive()
        if "log" in msg:
            if print_logs:
                print(f"Device log: {msg['log']}", end="")
        elif "materialID" in msg and format_measurements:
            print(f"\n\n\n*Measurement received*")
            if msg['metric'] < 0:
                print(f"An error has occurred: {msg['materialName']}")
            elif 0.95 < msg['signalIntensity'] < 1.01:
                print(f"No sample present (signal intensity {msg['signalIntensity']:.2f}).")
            elif msg['signalIntensity'] < 0.03:
                print(f"Non-transparent sample or low signal intensity ({msg['signalIntensity']:.2f}).")
            else:
                print(f"Material: {msg['materialName']}")
                print(f"Certainty: {msg['metric']:.2f}")
                print(f"Measurement took: {time.time()-request_time:3f} sec")
                print(f"Signal intensity: {int(100*msg['signalIntensity'])}%")
                if "weight" in msg:
                    print(f"Weight: {msg['weight']:.3f}kg")
                if "barcode" in msg:
                    print(f"Barcode: {msg['barcode']}")
                if "rfid" in msg:
                    print(f"RFID: {msg['rfid']}")
                if "sampleCounter" in msg and msg["sampleCounter"] > 0:
                    print(f"Sample counter: {msg['sampleCounter']}")
                if print_logs:
                    print(msg)
            measurement_requested = False
            print("Will wait for 2 seconds...")
            time.sleep(2)
        elif "state" in msg:
            print(f"State: {msg['state']}")
        else:
            print(msg)

    # Keep requesting measurements forever
    if request_measurements and (not measurement_requested or (time.time()-request_time) > 5):
        # Check the device is idle
        status = matoha_dev.get_status()

        if "state" not in status:
            print("The device is not responding.")
            time.sleep(0.5)
            continue

        if status["state"] == "WAITING_FOR_TRIGGER":
            print("The device was in auto-trigger mode, stopping it.")
            matoha_dev.stop_measuring()
        elif status["state"] == "IDLE":
            matoha_dev.single_measurement()
            print("\n\nTriggering a new measurement!")
            request_time = time.time()
            measurement_requested = True
            start = time.time()


matoha_dev.terminate_comms()
