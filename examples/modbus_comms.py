import os
import sys
from time import sleep
from serial.tools import list_ports
sys.path.append(os.path.realpath('.'))
from mcl.matoha_modbus import MatohaModbus

# Example usage: python3 modbus_comms.py
# This example uses a RS485 USB dongle such as this one https://www.amazon.co.uk/dp/B078X5H8H7
# connected to our sensing modules over RS485.

port_list = list_ports.grep(r"ttyUSB|usbserial|COM", include_links=False)
print("Available Modbus USB ports:")
serial_port = None

for port in port_list:
    serial_port = port.device
    print(port)

print("Choosing the first port in the list. (You can override this by editing the script)")
# or override the port name
#serial_port = "/dev/tty.usbserial-140"

if not serial_port:
    raise RuntimeError("No serial ports detected")

# Edit this as needed
address = 81  # Modbus address
rtu = False  # Use RTU mode (=True) or ASCII mode (=False)
baudrate = 115200  # RS485 Baud rate


# Print logs will print details about Modbus traffic - set to False for a cleaner output
m = MatohaModbus(serial_port, address=address, rtu=rtu, baudrate=baudrate, print_logs=False)

# Read the status
status = m.read_status()
print(f"Status: \n{status}")

# Typical response (status):
# {'flags': {'firmwareUpdatesAvailable': True, 'configUpdatesAvailable': False, 'libraryUpdatesAvailable': False,
#  'wifiConnected': True, 'bluetoothConnected': False, 'calibrationMode': False, 'wavelengthCalibrationMode': False,
#  'resultAvailable': False}, 'state': 2, 'acquisitionNumber': 4, 'sensorCount': 1, 'updateProgressPerc': 0,
#  'ipAddress': '192.168.0.105', 'serialNo': 'f432fbdeac01', 'firmwareVersion': '5ba4fc2-d'
#  }

# Stop the machine from measuring automatically (in case the autotrigger was enabled)
m.stop_measuring()
sleep(1)

while m.read_status()['state'] != 2:
    # Wait for the machine to stop measuring or calibrating (state 2 is calibrating)
    sleep(0.1)

# Trigger a single measurement
m.single_measurement()
print("\nSingle measurement triggered...\n")
measurement_result = m.wait_for_measurement(timeout=10, poll_interval=0.05)
print("Result:")
print(measurement_result)

# Typical response (measurement_result):
# {'materialID': 26, 'metric': 1.42, 'minSignal': 5.83, 'maxSignal': 6.91, 'sensorType': 0, 'wavelengthMin': 1550,
#  'wavelengthMax': 1950, 'signalIntensity': 0.2, 'sensorTemperature': 27.21, 'spectrumQuality': 1.0,
#  'materialName': 'RUB'
# }