# *Communications example using the Matoha BLE drivers*
#
# (c) 2021-22 Matoha Instrumentation Ltd, Licensed under the MIT license

# Warning - BLE support is in beta
# This code has only been tested on Mac OS X (M1, 11.2) and Raspberry Pi 3 but should work for Windows and Linux as well
# If you face any issues, please let us know and we'll do our best to help

import asyncio
import bleak
from bleak import BleakScanner
import sys
import os
import colorama
sys.path.append(os.path.realpath('.'))
from mcl.matoha_ble import MatohaBLE


async def scan_for_devices():
    found_devices = await BleakScanner.discover()
    matoha_devices = []
    for d in found_devices:
        if d.name and ("MATOHA" in d.name or "Matoha" in d.name or "Plast" in d.name or "Fabri" in d.name):
            matoha_devices.append(d)
            print(f"Found device {d.name} {d.address} {d.metadata}, RSSI {d.rssi}")

    return matoha_devices


def on_measurement(msg):
    """
    Callback function to parse measurement result
    :param msg: measurement result dict
    :return:
    """
    print(msg)

    print(f"\n\n\n*Measurement received*")
    if 'metric' not in msg or msg['metric'] < 0:
        print(colorama.Fore.RED + f"An error has occurred: {msg['materialName']}" + colorama.Fore.BLACK)
    else:
        print(colorama.Fore.GREEN + f"Material: {msg['materialName']}" + colorama.Fore.BLACK)
        print(colorama.Fore.GREEN + f"Certainty: {msg['metric']:.2f}" + colorama.Fore.BLACK)


if __name__ == "__main__":
    print("Scanning for BLE devices")
    loop = asyncio.get_event_loop()
    devices = loop.run_until_complete(scan_for_devices())

    if not devices:
        print("No devices found, exiting.")
        exit(1)

    # Pick the closest device - edit this as required
    closest_device = None
    highest_rssi = -1000
    for d in devices:
        if d.rssi > highest_rssi:
            closest_device = d
            highest_rssi = d.rssi

    print(f"Connecting to device {closest_device}")
    communicator = MatohaBLE(closest_device)

    try:
        status = loop.run_until_complete(communicator.get_status())
    except bleak.exc.BleakDBusError as e:
        print("\n\n**BLE library error. Please check no other device is connected to the instrument?**\n\n")
        raise e

    print(f"Device status: {status.to_dict()}")

    # Subscribe to measurements and wait for results indefinitely
    devices = loop.run_until_complete(communicator.subscribe_measurement(on_measurement, timeout=60))

    print("Disconnected from measurements.")

    # Restart the device to demonstrate the sending of commands
    print("Restarting the device")
    loop.run_until_complete(communicator.restart_device())