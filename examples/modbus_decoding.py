# Helper script to decode Modbus response - you can write the response you received
# from the device in the result_raw variable and run this script to decode it.

# Write your response here
# Note this response is imperfect and missing some fields, to test the error handling
result_raw = [302,41,1131,1417,0,1550,1950,20,3283,100,14135,8229,28535,27759,12832,9523,28704,27759,121,0]

result_register_map = {
    "materialID": {"address": 0, "type": int, "size": 1},
    "metric": {"address": 1, "type": float, "size": 1},
    "minSignal": {"address": 2, "type": float, "size": 1},
    "maxSignal": {"address": 3, "type": float, "size": 1},
    "sensorType": {"address": 4, "type": int, "size": 1},
    "wavelengthMin": {"address": 5, "type": int, "size": 1},
    "wavelengthMax": {"address": 6, "type": int, "size": 1},
    "signalIntensity": {"address": 7, "type": float, "size": 1},
    "sensorTemperature": {"address": 8, "type": float, "size": 1},
    "spectrumQuality": {"address": 9, "type": float, "size": 1},
    "materialName": {"address": 10, "type": str, "size": 16},
    "material0Percentage": {"address": 26, "type": int, "size": 1},
}

result = {}

for key, value in result_register_map.items():
        if len(result_raw) < value["address"]:
            print(f"ERROR: Result register {key} not found in the response (it's too short)")
            continue

        address = value["address"]
        if value["type"] == int:
            result[key] = result_raw[value["address"]]
        elif value["type"] == float:
            result[key] = result_raw[value["address"]] / 100.0
        elif value["type"] == str:
            result[key] = ""
            for i in range(value["size"]):
                if address+i >= len(result_raw):
                    print(f"ERROR: Result register {key} trimmed (the response is too short)")
                    break

                result[key] += chr(result_raw[address+i] & 0xFF)
                result[key] += chr(result_raw[address+i] >> 8)

            result[key] = str(result[key]).rstrip('\x00')
        else:
            raise Exception(f"Unknown result register type: {value['type']}")

print(result)

