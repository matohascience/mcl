# Matoha Communication Library - Python

This library provides the Python interface for communicating with our devices.

## Protocols and interfaces

#### The protocol

We use [Protocol Buffers](https://en.wikipedia.org/wiki/Protocol_Buffers) to transport data in an efficient manner.

Our PB message definition which can be easily implemented across various programming languages can found in the
[matoha-message](https://gitlab.com/matohascience/matoha-message) repository. Please have a look into this repository
for the documentation of the protocol and the commands.

Currently supported interfaces are below. More will be added in the future; if you have specific requirements please
contact us.

#### CAN bus

[CAN bus](https://en.wikipedia.org/wiki/CAN_bus) is a widely use network protocol for communication across many nodes.
Requiring only two wires, it supports relatively high speed (250-1000 kbits/s) communications, with many nodes connected
to the same two physical wires. One drawback is that the packet payload size is restricted to 8 bytes (ignoring the CAN-FD
mode which is not supported by our spectrometers as it is covered by 3rd-party patents).

In order to send larger than 8-byte messages across the CAN bus, our spectrometers implement the
[ISO-TP](https://en.wikipedia.org/wiki/ISO_15765-2) transport layer. ISO-TP splits our transmission into 8-bytes chunks
and adds headers ensuring the whole message is received in the correct order. There are many libraries available that
implement ISO-TP, for instance the [Python library](https://github.com/pylessard/python-can-isotp) or the
[Arduino library](https://github.com/altelch/iso-tp). The Python library linked is used internally by Matoha for
this package.

#### UART/Serial/RS485 bus

Some of our devices (contact us for details) can communicate over UART/serial. This is interface can be also used to
connect to a computer via USB through a USB-to-serial converter such as Silabs CP2102N. The usual baud rate is
115200, with no hardware flow control.

Each of the messages starts with `{` and ends with `}`. This is to easily separate the different messages coming
from the device. Within the `{}`, the message is protobuf-encoded (exactly the same encoding as CAN and other interfaces),
see the section *Protocol* above. As protobuf messages are binary, to preserve good compatibility with serial terminals,
the protobuf message is additionally base-64 encoded. Finally, the message contains a CRC32 checksum to ensure the
message (and commands, in particular) are not corrupt.

The overall format is therefore as follows:
```
{messagedatahere===,1234123}
{ <--- start
  b64 protobuf message
                   , <--- separator
                    crc32 check (as unsigned 32-bit int)
                          } <--- end
```

Our library handles all of the encoding and decoding - please refer to the source code and examples.

#### Bluetooth LE

All of our devices act as Bluetooth LE GATT servers. This is the communication protocol used by our iOS/Android app and
thanks to the Python [Bleak library](https://github.com/hbldh/bleak) this Bluetooth interface is also accessible to
computers.

The protocol is based on the matoha-message format. However, the messages have to be split into packets because of the
MTU (max transmission unit) restrictions (typically 20-500 bytes). The receive format (from the device) is as follows:

Multi-part (message doesn't fit into the MTU):
- Byte 0 = 0xAA (fixed header)
- Byte 1 = 0xBB (fixed header)
- Byte 2 = 0xCC (fixed header)
- Byte 3 = 0xDD (fixed header)
- Byte 4 = packet number (incremented for each packet of this message; more than 255 packets are not supported)
- Byte 5 = total number of packets per this message
- Byte 6 = measurement ID (incremented for each measurement)
- Bytes 7-9 = reserved (send as 0) 
- Bytes 10+ = payload

Single-part (message fits into the MTU):
- Bytes 0+ = payload

Our library handles all of the encoding and decoding for you - please refer to the source code and examples.

#### MODBUS

Our OEM modules can optionally support Modbus over RS485 or TCP (Wi-Fi). This protocol is widely used in industrial
automation and enables connecting our spectrometers to PLCs, SCADA systems, etc. The protocol is further described in 
the [Modbus.md](Modbus.md) file.

#### Other buses and interfaces

It should be technically feasible to support other communication protocols. Please do not hesitate to contact us to
discuss your requirements.

## Basic usage

### Installation for development

- Ensure you have not installed mcl from pip
```pip uninstall mcl```
- Clone the repository, *including the submodules*.
```
git clone --recurse-submodules git@gitlab.com:matohascience/mcl.git
cd mcl
```
- (Optional but recommended) Create a virtual environment
```
python3 -m venv venv && source venv/bin/activate
```
- Install Python dependencies
```
pip3 install -r requirements.txt 
pip3 install -r mcl/matoha_message/requirements.txt
```
- Install a protobuf compiler (see the `mcl/matoha_message/README.md` file)
- Compile Matoha Message
```
cd mcl/matoha_message
./compile_protobufs.sh
cd ../../
```

### Pip pre-built package

If you would like a pre-built package with MCL, you can download it from our GitLab pip repository.
This doesn't include the examples folder but makes it easier to integrate with your own codebase.

`pip install mcl --extra-index-url https://gitlab.com/api/v4/projects/14068973/packages/pypi/simple`


### Running examples

Currently, two CAN interfaces are supported GS-CAN and python-can. Please choose the relevant example and open the file.
You may need to modify the configuration to match your setup (see the instructions inside the example). Finally,
you should be able to run the example, e.g.

`python3 examples/pythoncan_comms.py`

## Developer instructions

### Building the wheel

To create a distributable .whl, please run

```bash
pip3 install setuptools wheel twine
rm dist/*
python3 setup.py sdist bdist_wheel
```

The latest build shall be found in the dist/ folder as a .whl.

## License

(c) 2023 Matoha Instrumentation Ltd, licensed under the open-source MIT license.
See the LICENSE file for further details.