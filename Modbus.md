# Documentation for Modbus

Our OEM Sensing modules can optionally support Modbus over RS485 and in the future, TCP (Wi-Fi). This protocol is widely used in
industrial automation and enables connection to a wide range of devices. The protocol is supported by many libraries
and tools and here we use the Minimal Modbus library (https://minimalmodbus.readthedocs.io/en/master/).

## Communication parameters

Parameter | Default Value | Alternative values*
--- | --- | ---
Protocol | RS485 **ASCII** mode | RS485 **RTU** mode
Device address | 81 | 1-247
Baud rate | 115200 | 300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600
Parity | None | 
Stop bits | 1 |
Data bits | 8 |
Timeout | 0.5s |

*The alternative values can be set by Matoha - please contact us if you need to change them. We can do this remotely 
as well, as long as the device is connected to the internet.

## Modbus registers

### Status registers

All Status registers are "Input registers", that is, read-only. The appropriate Modbus command is
"4 - Read input registers."

Address | Name | Description | Size (uint16)
--- | --- | --- | ---
0 | flags | Flags register (see below) | 1
1 | state | State register (see below) | 1
2 | acquisitionNumber | Acquisition number - gets incremented with each measurement. | 1
3 | sensorCount | Number of sensors connected to the device. | 1
4 | updateProgressPerc | Firmware update progress percentage (0-100%) | 1
6 | ipAddress | IP address of the device (in uint32 format) | 2
8 | serialNo | Serial number of the device - in raw bytes which need to be converted to hex. | 4
12 | firmwareVersion | Firmware version of the device - in raw bytes which need to be converted to ASCII. | 5


**Flags register bits**

The flag register is 16-bit wide (address 0, see above), with the following contents:

Bit | Name | Description
--- | --- | ---
16-8 | reserved | Reserved
7 | resultAvailable | Set to 1 if a new measurement is available
6 | wavelengthCalibrationMode | Set to 1 if the device is in wavelength calibration mode
5 | calibrationMode | Set to 1 if the device is in calibration mode
4 | bluetoothConnected | Set to 1 if the device is connected to a Bluetooth LE client
3 | wifiConnected | Set to 1 if the device is connected to a Wi-Fi network
2 | libraryUpdatesAvailable | Set to 1 if there are library updates available
1 | configUpdatesAvailable | Set to 1 if there are configuration updates available
0 | firmwareUpdatesAvailable | Set to 1 if there are firmware updates available

**State register values**

The state register (address 1, see above) is a 16-bit wide register with the following values:

Value | Name | Description
--- | --- | ---
0 | UNKNOWN | Unknown state
1 | STARTING | The device is starting - no measurements possible.
2 | CALIBRATING | The device is calibrating - NO SAMPLE may be present.
3 | IDLE | The device is idle - ready to measure.
4 | MEASURING | The device is currently measuring.
5 | ERROR | The device is in error state.
6 | WAITING_FOR_TRIGGER | The device is waiting for a trigger event (sample being placed or manual trigger).
7 | SLEEPING | The device is sleeping, you can wake it up with a WAKE UP command.
8 | UPDATING | The device is updating - DO NOT INTERRUPT.

### Result registers

All Result registers are "Input registers", that is, read-only. The appropriate Modbus command is
"4 - Read input registers."

Some of the values are floating-point numbers, which are stored as a 16-bit integer in the Modbus registers. To convert
the integer to a floating-point number, divide the integer by 100. These are labelled as "*100" in the table below.

| Address | Name                | Description                                                                                    | Size (uint16) |
|---------|---------------------|------------------------------------------------------------------------------------------------|---------------|
| 100     | materialID          | Material ID (see below)                                                                        | 1             |
| 101     | metric              | Metric*100 - the higher it is, more confident the machine is in the identity. <0.4 is Unknown. | 1             |
| 102     | minSignal           | Minimum signal value in nA*100                                                                 | 1             |
| 103     | maxSignal           | Maximum signal value in nA*100                                                                 | 1             |
| 104     | sensorType          | Sensor type (0 for our standard type)                                                          | 1             |
| 105     | wavelengthMin       | Minimum wavelength in nm                                                                       | 1             |
| 106     | wavelengthMax       | Maximum wavelength in nm                                                                       | 1             |
| 107     | signalIntensity     | Signal intensity in nA*100                                                                     | 1             |
| 108     | sensorTemperature   | Sensor temperature in °C*100                                                                   | 1             |
| 109     | spectrumQuality     | Spectrum quality*100, typically above 7 is good quality                                        | 1             |
| 110     | materialName        | Material name string (16 bytes)                                                                | 16            |
| 126     | material0Percentage | Percentage of the first material in the sample                                                 | 1             |

#### Material IDs (plastics)
The plastic material IDs are as follows: (not all materials are supported yet)

| Material ID | Material name |
|-------------|---------------|
| 1           | PET           |
| 2           | PE            |
| 3           | PVC           |
| 5           | PP            |
| 6           | PS            |
| 7           | PC            |
| 8           | ABS           |
| 9           | PMMA          |
| 10          | PA            |
| 11          | PLA           |
| 12          | PETG          |
| 13          | PU            |
| 14          | PVDF          |
| 15          | PEEK          |
| 16          | POM           |
| 17          | PAI           |
| 18          | ASA           |
| 19          | TPU           |
| 20          | PSU           |
| 21          | PEI           |
| 22          | SAN           |
| 23          | EVA           |
| 24          | NBR           |
| 25          | SIL           |
| 26          | RUB           |
| 27          | NEO           |
| 28          | HDPE          |
| 29          | LDPE          |
| 30          | PA6           |
| 31          | PA66          |
| 32          | PA12          |
| 33          | PA612         |
| 34          | PA11          |
| 35          | PBT           |
| 36          | TPV           |
| 37          | TPE           |
| 38          | ABS-PC        |
| 39          | PPSU          |
| 40          | SBC           |
| 41          | SMMA          |
| 42          | SBR           |
| 43          | TPES          |
| 44          | TPC           |
| 45          | PHBV          |
| 46          | PCL           |
| 47          | PBS           |
| 48          | PBAT          |
| 50          | Paper         |

#### Material IDs (fabrics)

The material IDs for fabrics are as follows:

| Material ID | Material          |
|-------------|-------------------|
| 1           | Cotton            |
| 2           | Polyester         |
| 3           | Wool              |
| 4           | Acrylic           |
| 5           | Nylon (polyamide) |
| 6           | Elastane          |
| 7           | Silk              |
| 8           | Viscose           |
| 9           | Acetate           |
| 16          | Polypropylene     |

The material ID is then calculated as:
```
materialID = 100 * major component ID + minor component ID
```
For instance, 100% polyester material ID is 200. 60-40% polyester-cotton blend is 201.


### Command registers

All Command registers are "Holding registers", that is, read-write. However, a read has no effect.
The appropriate Modbus command is "6 - Write input registers."

Address | Name | Description | Size (uint16)
--- | --- | --- | ---
0 | COMMAND | Main command register (see below) | 1

To execute a command, write a value (see table below) into the COMMAND register.

**Commands**

Value | Name | Description
--- | --- | ---
0 | NONE | No command
1 | STOP_MEASURING | Stop measuring (disable autotrigger).
2 | START_MEASURING | Start measuring (enable autotrigger).
3 | SINGLE_MEASUREMENT | Perform a single measurement.
4 | RESTART | Restart the device.
5 | reserved | No effect in Modbus operation.
6 | SLEEP | Put the device to sleep.
7 | WAKE_UP | Wake up the device.
8 | RUN_OTA | Run the OTA firmware update.
