#!/usr/bin/env python

import setuptools

setuptools.setup(
    name='mcl',
    version='0.6.11',
    description='Matoha Communication Library',
    author='Matoha Instrumentation Ltd.',
    author_email='hello@matoha.com',
    url='https://gitlab.com/matohascience/mcl',
    packages=setuptools.find_packages(),
    license='MIT license',
    classifiers=["Programming Language :: Python :: 3",
               "License :: OSI Approved :: MIT License",
               "Operating System :: OS Independent"],
    python_requires='>=3.6',
    install_requires=[
        'can-isotp>=1.6',
        'protobuf==3.20.1',
        'gs-usb>=0.2.8',
        'python-can>=3.3.3',
        'pyserial>=3.4',
        'colorama>=0.4',
        'bleak==0.19.5',
        'minimalmodbus>=2.0.1'
    ],
    long_description=open('README.md').read(),
)
