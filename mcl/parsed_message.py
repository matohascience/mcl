from mcl.matoha_message import matoha_message_pb2
from google.protobuf.json_format import MessageToDict
import base64


class ParsedMessage:
    """
    Helper class for parsing the messages received from the device.
    """

    def __init__(self, data=None):
        """
        Create the message.

        :param data: bytes data to decode (optional
        """
        self._pb = matoha_message_pb2.MatohaMessage()
        if data:
            try:
                self._pb.ParseFromString(data)
            except Exception as e:
                raise RuntimeError(f"Message parsing from protobuf failed, {e}. Raw data:\n {data.hex()}")

    def to_dict(self, remove_colouring=False):
        """
        Export the message as a Python dictionary.

        :param remove_colouring: remove Shell colouring
        :return: dictionary containing the message parameters; see the matoha-message repo documentation
        """
        return_dict = MessageToDict(self._pb)
        if "spectrum" in return_dict:
            return_dict["spectrum"] = self._parse_spectrum(return_dict)
        if "log" in return_dict:
            if remove_colouring:
                return_dict["log"] = return_dict["log"].replace('\x1b[0m', '').replace('\x1b[0;31m', '') \
                    .replace('\x1b[0;33m', '').replace('\x1b[0;32m', '')

        return return_dict

    @staticmethod
    def _parse_spectrum(data):
        """
        Decode the spectrum into a numerical array

        :param data: encoded spectrum
        :return: decoded spectrum, an numerical array
        """
        if "spectrum" not in data or not data["spectrum"] or "spectrumPoints" not in data or data["spectrumPoints"] < 1:
            return

        spec = base64.b64decode(data["spectrum"])
        bytes_per_point = int(len(spec) / data["spectrumPoints"])

        processed_spectrum = []
        for i in range(0, len(spec), bytes_per_point):
            val = 0
            for j in range(bytes_per_point):
                val += spec[i + j] * (2 ** (8 * j))
            processed_spectrum.append(val)

        # normalise
        minval = min(processed_spectrum)
        maxval = max(processed_spectrum)
        offset = minval
        amplitude = maxval - minval

        # prevent division by zero
        if amplitude == 0:
            amplitude = 1.0

        for a in range(len(processed_spectrum)):
            processed_spectrum[a] = round(((processed_spectrum[a] - offset) / amplitude), 5)

        return processed_spectrum
