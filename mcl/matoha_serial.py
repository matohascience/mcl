import serial
import time
import threading
import base64
from .parsed_message import ParsedMessage
from .matoha_message import matoha_message_pb2
import binascii

MESSAGE_START = '{'
MESSAGE_END = '}'

exitReqestEvent = threading.Event()

class MatohaSerial:
    """
    Main driver for communicating with Matoha devices over UART / serial.
    """

    def __init__(self, port, baudrate=115200, print_logs=False):
        """
        Initialisation of the Matoha device serial comms driver.

        :param port: string with the USB port name, e.g. "/dev/ttyACM0" on Linux
        :param baudrate: baudrate - our instrument default is 115200
        :param print_logs: print device logs coming over the serial
        """
        self._port = serial.Serial()
        self._port.port = port
        self._port.baudrate = baudrate
        self._port.rts = False
        self._port.dtr = False
        self._port.open()
        time.sleep(3)  # This sleep is needed to give enough opportunity for the device to boot up
        self._buffer = []
        self._buffer_msg_start = -1
        self._buffer_msg_end = -1
        self._messages = []
        self.print_logs = print_logs

    def start_comms(self):
        """
        Sets up the communications.

        :return: None
        """
        exitReqestEvent.clear()
        self.thread = threading.Thread(target=self.thread_task)
        self.thread.start()

    def stop_comms(self):
        """
        Stops the communications.

        :return: None
        """
        # if self.print_logs:
        #     print("Stop request....")
        exitReqestEvent.set()
        time.sleep(0.1)
        self._port.close()
        if self.thread.is_alive():
            try:
                self.thread.join()
            except RuntimeError:
                pass

    def __del__(self):
        """
        Destructor, stops the communications.

        :return: None
        """
        self.stop_comms()

    def thread_task(self):
        """
        Periodically executed task checking for exit flags and incoming data

        :return: None
        """
        while not exitReqestEvent.is_set():
            recv_char = self._port.read()
            while recv_char and not exitReqestEvent.is_set():
                try:
                    recv_char = recv_char.decode('utf-8')
                except:
                    # Probably rubbish, continue
                    recv_char = self._port.read()
                    continue

                # Start delimiter received
                if recv_char == MESSAGE_START:
                    self._buffer = []
                    self._buffer_msg_start = 0
                    self._buffer.append(recv_char)

                # End delimiter received, process the message
                elif recv_char == MESSAGE_END:
                    self._buffer_msg_end = len(self._buffer)
                    self._buffer.append(recv_char)
                    try:
                        # Convert to a string then base-64 decode
                        if self.print_logs:
                            print("RECEIVED: " + "".join(self._buffer[1:-1]))
                        split_data = "".join(self._buffer[1:-1]).split(',')
                        data = split_data[0]
                        crc = int(split_data[1])
                        expected_crc = binascii.crc32(data.encode('utf-8'))
                        if crc != expected_crc:
                            print(f"Error - CRC mismatch expected {expected_crc} got {crc}.")
                        else:
                            decoded = base64.b64decode(data)
                            self._messages.append(decoded)
                    except Exception as e:
                        print("Error - Message decoding failed ", e)
                    self._buffer = []
                    self._buffer_msg_start = -1
                    self._buffer_msg_end = -1

                elif recv_char == '\n':
                    # Received a newline = a log message came in
                    if self.print_logs:
                        print("".join(self._buffer))
                    self._buffer = []
                    self._buffer_msg_start = -1
                    self._buffer_msg_end = -1
                else:
                    self._buffer.append(recv_char)

                # Buffer too big - probably full of rubbish so empty
                if len(self._buffer) > 5000:
                    self._buffer = []
                    self._buffer_msg_end = -1
                    self._buffer_msg_start = -1

                recv_char = self._port.read()
            time.sleep(0.02)

    def available(self):
        """
        Check if new messages have been sent by the device

        :return: True if message(s) are available
        """
        return len(self._messages) > 0

    def receive(self):
        """
        Receive the new messages, nicely formatted by Matoha codes.

        :return: parsed dictionary, see the matoha-message repo documentation
        """
        return ParsedMessage(self._messages.pop(0)).to_dict()

    def _send_command(self, command):
        msg = matoha_message_pb2.MatohaMessage()
        msg.command = command
        payload = base64.b64encode(msg.SerializeToString())
        crc = binascii.crc32(payload)
        if self.print_logs:
            print('SENDING {' + payload.decode('utf-8') + ',' + str(crc) + '}')
        self._port.write(b'{' + payload + b',' + str(crc).encode('utf-8') + b'}')

    def start_measuring(self):
        """
        Enable the measurement auto-trigger.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.START_MEASURING)

    def stop_measuring(self):
        """
        Disable the measurement auto-trigger.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.STOP_MEASURING)

    def single_measurement(self):
        """
        Force a measurement, regardless of the auto-trigger. We recommend stopping the measurement before (stop_measuring())

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.SINGLE_MEASUREMENT)

    def restart_device(self):
        """
        Restart the device.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.RESTART)

    def request_status(self):
        """
        Request the device status.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.SEND_STATUS)

    def get_status(self):
        # Request the status to be printed on the terminal
        self.request_status()

        start = time.time()
        while (time.time() - start) < 5:
            if self.available():
                msg = self.receive()
                if msg and "state" in msg:
                    return msg

        print("Error: status request timed out")
        return {}

    def sleep(self):
        """
        Set the instrument into sleep mode to save power. Call wake_up before continuing to measure.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.SLEEP)

    def wake_up(self):
        """
        Wake up the instrument to resume measurements.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.WAKE_UP)

    def run_ota(self):
        """
        Run over-the-air firmware update of the instrument. Important - do not disturb the instrument for about
        5 minutes after, it will restart itself when the OTA is done and load the new firmware.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.RUN_OTA)
