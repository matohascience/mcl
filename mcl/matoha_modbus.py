from .matoha_message import matoha_message_pb2
import minimalmodbus
from time import time, sleep


flags_map = {  # Use to decode the flags register
    "firmwareUpdatesAvailable": 0,
    "configUpdatesAvailable": 1,
    "libraryUpdatesAvailable": 2,
    "wifiConnected": 3,
    "bluetoothConnected": 4,
    "calibrationMode": 5,
    "wavelengthCalibrationMode": 6,
    "resultAvailable": 7,
}

status_register_map = {
    "flags": {"address": 0, "type": int, "size": 1},
    "state": {"address": 1, "type": int, "size": 1},
    "acquisitionNumber": {"address": 2, "type": int, "size": 1},
    "sensorCount": {"address": 3, "type": int, "size": 1},
    "updateProgressPerc": {"address": 4, "type": int, "size": 1},
    "ipAddress": {"address": 6, "type": int, "size": 2},
    "serialNo": {"address": 8, "type": int, "size": 4},
    "firmwareVersion": {"address": 12, "type": str, "size": 5}
}

result_register_offset = 100  # All the result register addresses should be offset by this much
result_register_map = {
    "materialID": {"address": 0, "type": int, "size": 1},
    "metric": {"address": 1, "type": float, "size": 1},
    "minSignal": {"address": 2, "type": float, "size": 1},
    "maxSignal": {"address": 3, "type": float, "size": 1},
    "sensorType": {"address": 4, "type": int, "size": 1},
    "wavelengthMin": {"address": 5, "type": int, "size": 1},
    "wavelengthMax": {"address": 6, "type": int, "size": 1},
    "signalIntensity": {"address": 7, "type": float, "size": 1},
    "sensorTemperature": {"address": 8, "type": float, "size": 1},
    "spectrumQuality": {"address": 9, "type": float, "size": 1},
    "materialName": {"address": 10, "type": str, "size": 16},
    "material0Percentage": {"address": 26, "type": int, "size": 1},
}


class MatohaModbus:
    """
    Main driver for communicating with Matoha devices over Modbus.
    """

    def __init__(self, port, baudrate=115200, address=81, rtu=False, print_logs=False):
        """
        Initialisation of the Matoha device serial comms driver.

        :param port: string with the USB port name, e.g. "/dev/ttyACM0" on Linux
        :param baudrate: baudrate - our instrument default is 115200
        :param address: Modbus address of the device
        :param rtu: use RTU mode instead of ASCII
        :param print_logs: print device logs coming over the serial, enables debug mode for Modbus traffic
        """

        self.instrument = minimalmodbus.Instrument(port, address, minimalmodbus.MODE_RTU if rtu else minimalmodbus.MODE_ASCII, debug=print_logs)
        self.instrument.serial.baudrate = baudrate
        self._buffer = []
        self._buffer_msg_start = -1
        self._buffer_msg_end = -1
        self._messages = []
        self.print_logs = print_logs

    def read_status(self):
        """
        Read the device status registers.

        :return: dictionary with the status register contents
        """
        status = {}
        
        # Find the starting address and the total read length
        start_address = 65535
        end_address = 0
        for key, value in status_register_map.items():
            if value["address"] < start_address:
                start_address = value["address"]
            if (value["address"] + value["size"]) > end_address:
                end_address = value["address"] + value["size"]

        # Read all the status registers at the same time
        status_raw = self.instrument.read_registers(start_address, end_address-start_address, functioncode=4)

        for key, value in status_register_map.items():
            address = value["address"]
            if key == "ipAddress":
                # The IP address is stored as 4 bytes so convert them to formatted numbers
                status[key] = f"{status_raw[address] & 0xFF}.{status_raw[address] >> 8}.{status_raw[address+1] & 0xFF}.{status_raw[address+1] >> 8}"
            elif key == "serialNo":
                # The serial number is encoded in 6 bytes - normally shown as 12 hex digits
                status[key] = f"{status_raw[address+2]:04x}{status_raw[address+1]:04x}{status_raw[address]:04x}"
            elif key == "flags":
                # The flags register is encoded as a bitfield
                status[key] = {}
                for flag_key, flag_value in flags_map.items():
                    status[key][flag_key] = bool(status_raw[address] & (1 << flag_value))
            elif value["type"] == int:
                status[key] = status_raw[value["address"]]
            elif value["type"] == str:
                # The firmware version is encoded as a string
                status[key] = ""
                for i in range(value["size"]):
                    status[key] += chr(status_raw[address+i] & 0xFF)
                    status[key] += chr(status_raw[address+i] >> 8)
                status[key] = status[key].rstrip('\x00')

            else:
                raise Exception(f"Unknown status register type: {value['type']}")

        return status

    def read_measurement_result(self):
        """
        Read the device measurement registers.

        :return: dictionary with the measurement register contents
        """
        result = {}

        # Find the starting address and the total read length
        start_address = 65535
        end_address = 0
        for key, value in result_register_map.items():
            if value["address"] < start_address:
                start_address = value["address"]
            if (value["address"] + value["size"]) > end_address:
                end_address = value["address"] + value["size"]

        # Read all the result registers at the same time
        result_raw = self.instrument.read_registers(start_address + result_register_offset, end_address-start_address,
                                                    functioncode=4)

        for key, value in result_register_map.items():
            address = value["address"]
            if value["type"] == int:
                result[key] = result_raw[value["address"]]
            elif value["type"] == float:
                result[key] = result_raw[value["address"]] / 100.0
            elif value["type"] == str:
                result[key] = ""
                for i in range(value["size"]):
                    result[key] += chr(result_raw[address+i] & 0xFF)
                    result[key] += chr(result_raw[address+i] >> 8)

                result[key] = str(result[key]).rstrip('\x00')
            else:
                raise Exception(f"Unknown result register type: {value['type']}")

        return result

    def wait_for_measurement(self, timeout=10, poll_interval=0.05):
        """
        Blocking call which returns upon a new measurement or a timeout

        :param timeout: timeout in seconds, will return if the measurement doesn't complete within this time
        :param poll_interval: will check for new measurement in this interval
        :return: None if timeout or dict with the measurement result
        """
        start = time()
        while time() - start < timeout:
            status = self.read_status()
            if status["flags"]["resultAvailable"]:
                return self.read_measurement_result()
            sleep(poll_interval)

        return None  # timeout

    def _send_command(self, command):
        """

        :param command:
        :return:
        """
        return self.instrument.write_register(0, command, functioncode=6)

    def start_measuring(self):
        """
        Enable the measurement auto-trigger.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.START_MEASURING)

    def stop_measuring(self):
        """
        Disable the measurement auto-trigger.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.STOP_MEASURING)

    def single_measurement(self):
        """
        Force a measurement, regardless of the auto-trigger. We recommend stopping the measurement before (stop_measuring())

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.SINGLE_MEASUREMENT)

    def restart_device(self):
        """
        Restart the device.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.RESTART)

    def request_status(self):
        """
        Request the device status.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.SEND_STATUS)

    def sleep(self):
        """
        Set the instrument into sleep mode to save power. Call wake_up before continuing to measure.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.SLEEP)

    def wake_up(self):
        """
        Wake up the instrument to resume measurements.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.WAKE_UP)

    def run_ota(self):
        """
        Run over-the-air firmware update of the instrument. Important - do not disturb the instrument for about
        5 minutes after, it will restart itself when the OTA is done and load the new firmware.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.RUN_OTA)
