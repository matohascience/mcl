# Matoha BLE drivers
#
# (c) 2021 Matoha Instrumentation Ltd, Licensed under the MIT license


import logging
import time
from bleak import BleakClient
import asyncio
from .parsed_message import ParsedMessage
from .matoha_message import matoha_message_pb2
from bleak import _logger as logger

CHARACTERISTIC_UUID = "54B476DB-4A2E-4489-858C-BB0A98387DBC"

class MatohaBLE:
    """
    Main driver for communicating with Matoha devices over Bluetooth LE.
    """

    def __init__(self, ble_device):
        """
        Initialisation of the Matoha device BLE comms driver.

        :param ble_device: BLE device address (use Bleak scan to obtain it, see the examples)
        """
        if not ble_device:
            raise RuntimeError("Please specify the BLE device")
        self.ble_device = ble_device

        # Properties for processing the incoming messages
        self.last_measurement_id = -1
        self.multipart_buffer = b""
        self.multiparts_received = []
        self.measurement_callback = None

    async def subscribe_measurement(self, callback=None, timeout=10000):
        """
        To be called by asyncio to run all the communications.

        :param callback: call this function with the measurement result
        :param timeout: timeout for the measurement in seconds
        :return: None
        """
        async with BleakClient(self.ble_device) as client:
            self.measurement_callback = callback
            await client.start_notify(CHARACTERISTIC_UUID, self.on_message)
            print("Connected to the device and waiting for messages.")
            await asyncio.sleep(timeout)
            self.measurement_callback = None
            #await client.stop_notify(CHARACTERISTIC_UUID)

    @staticmethod
    def is_multipart(message: bytes) -> bool:
        """
        Check if it is a multipart message (has a header with the first 4 bytes 0xAA 0xBB 0xCC 0xDD)

        Header (10 bytes)
        4 x Multipart warning bytes 0xAA 0xBB 0xCC 0xDD
        Frame ID byte
        Total number of chunks byte
        Measurement counter byte
        Padding to 10 bytes

        :param message: message bytes
        :return: True if multipart
        """

        if len(message) <= 10: # the header is 10 bytes so the message must be larger
            return False

        return message[0] == 0xAA and message[1] == 0xBB and message[2] == 0xCC and message[3] == 0xDD

    def on_message(self, sender, data):
        """Simple notification handler which prints the data received."""
        #print(f"Received {len(data)} bytes: {data}")

        if self.is_multipart(data):
            partNo = data[4]
            totalParts = data[5]
            measurementId = data[6]
            print(f"Multipart message, part {partNo+1} out of {totalParts} meas no {measurementId}")

            # Is this a new measurement?
            if measurementId != self.last_measurement_id:
                self.multipart_buffer = bytes(data[10:])
                self.last_measurement_id = measurementId
                self.multiparts_received = [False]*totalParts
                self.multiparts_received[partNo] = True
            else:
                if not self.multiparts_received[partNo]:
                    self.multipart_buffer += bytes(data[10:])
                    self.multiparts_received[partNo] = True

            print(f"Multipart status {self.multiparts_received}")

            # Received in full? (all True?)
            if self.multiparts_received.count(True) == len(self.multiparts_received):
                print("Received in full")
                msg = ParsedMessage(self.multipart_buffer).to_dict()
                if self.measurement_callback:
                    self.measurement_callback(msg)

        else:  # Single-part message
            msg = ParsedMessage(data).to_dict()
            if self.measurement_callback:
                self.measurement_callback(msg)

    async def _send_command(self, command):
        msg = matoha_message_pb2.MatohaMessage()
        msg.command = command

        async with BleakClient(self.ble_device) as client:
            await client.write_gatt_char(CHARACTERISTIC_UUID, data=msg.SerializeToString(), response=True)
            await asyncio.sleep(0.2)

    async def get_status(self):
        """
        Retrieve the current status
        :return: status dict
        """
        async with BleakClient(self.ble_device) as client:
            msg = await client.read_gatt_char(CHARACTERISTIC_UUID)
            return ParsedMessage(msg)

    async def start_measuring(self):
        """
        Enable the measurement auto-trigger.

        :return: None
        """
        await self._send_command(matoha_message_pb2.MatohaMessage.command_t.START_MEASURING)

    async def stop_measuring(self):
        """
        Disable the measurement auto-trigger.

        :return: None
        """
        await self._send_command(matoha_message_pb2.MatohaMessage.command_t.STOP_MEASURING)

    async def single_measurement(self):
        """
        Force a measurement, regardless of the auto-trigger. We recommend stopping the measurement before (stop_measuring())

        :return: None
        """
        await self._send_command(matoha_message_pb2.MatohaMessage.command_t.SINGLE_MEASUREMENT)

    async def restart_device(self):
        """
        Restart the device.

        :return: None
        """
        await self._send_command(matoha_message_pb2.MatohaMessage.command_t.RESTART)

    async def sleep(self):
        """
        Set the instrument into sleep mode to save power. Call wake_up before continuing to measure.

        :return: None
        """
        await self._send_command(matoha_message_pb2.MatohaMessage.command_t.SLEEP)

    async def wake_up(self):
        """
        Wake up the instrument to resume measurements.

        :return: None
        """
        await self._send_command(matoha_message_pb2.MatohaMessage.command_t.WAKE_UP)

    async def run_ota(self):
        """
        Run over-the-air firmware update of the instrument. Important - do not disturb the instrument for about
        5 minutes after, it will restart itself when the OTA is done and load the new firmware.

        :return: None
        """
        await self._send_command(matoha_message_pb2.MatohaMessage.command_t.RUN_OTA)
        print("Started OTA. This will take about 5 min.")

