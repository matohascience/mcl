import isotp
import time
import threading
from .parsed_message import ParsedMessage
from .matoha_message import matoha_message_pb2


class MatohaCAN:
    """
    Main driver for communicating with Matoha devices over CAN with ISO-TP transport layer.
    """

    def __init__(self, can_tx_address, can_rx_address, rx_function, tx_function, setup_function=None,
                 shutdown_function=None):
        """
        Initialisation of the Matoha device CAN comms driver.

        Please populate the arguments with appropriate functions specific to your CAN board - these are dependent
        on your particular setup. Please see the examples for the rx/tx_functions for some common CAN drivers

        :param can_tx_address: 11-bit CAN address for ISO-TP transmissions
        :param can_rx_address: 11-bit CAN address for ISO-TP receptions
        :param rx_function: receive function handle which takes no arguments, reads from the CAN bus and returns an isotp.CanMessage object
        :param tx_function: transmit function handle which takes an isotp.CanMessage object as the arguments and transmits the message over the CAN bus
        :param setup_function: CAN bus initialisation function handle, optional
        :param shutdown_function: CAN bus shutdown function handle, optional
        """
        self.setup_function = setup_function
        self.shutdown_function = shutdown_function
        self.exit_requested = False
        addr = isotp.Address(isotp.AddressingMode.Normal_11bits, rxid=can_rx_address, txid=can_tx_address)
        self.layer = isotp.TransportLayer(rxfn=rx_function, txfn=tx_function, address=addr, error_handler=self._error_handler)

    def start_comms(self):
        """
        Sets up the communications.

        :return: None
        """
        if self.setup_function:
            self.setup_function()
        self.exit_requested = False
        self.thread = threading.Thread(target=self.thread_task)
        self.thread.start()

    def stop_comms(self):
        """
        Stops the communications.

        :return: None
        """
        self.exit_requested = True
        if self.thread.is_alive():
            self.thread.join()

    def _error_handler(self, error):
        """
        Custom error handler, populate as needed

        :param error: CAN error
        :return: None
        """
        print(f"CAN Error! {error}")

    def thread_task(self):
        """
        Periodically executed task checking for exit flags and incoming data

        :return: None
        """
        while not self.exit_requested:
            self.layer.process()  # Non-blocking
            time.sleep(self.layer.sleep_time())  # Variable sleep time based on state machine state

    def terminate_comms(self):
        """
        Terminate the communications with the device / CAN-USB adapter.

        :return: None
        """
        self.stop_comms()
        if self.shutdown_function:
            self.shutdown_function()

    def available(self):
        """
        Check if new messages have been sent by the device

        :return: True if message(s) are available
        """
        return self.layer.available()

    def receive(self):
        """
        Receive the new messages, nicely formatted by Matoha codes.

        :return: parsed dictionary, see the matoha-message repo documentation
        """
        data = self.layer.recv()
        parsed = ParsedMessage(data)
        return parsed.to_dict()

    def _send_command(self, command):
        msg = matoha_message_pb2.MatohaMessage()
        msg.command = command
        self.layer.send(msg.SerializeToString())

    def start_measuring(self):
        """
        Enable the measurement auto-trigger.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.START_MEASURING)

    def stop_measuring(self):
        """
        Disable the measurement auto-trigger.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.STOP_MEASURING)

    def single_measurement(self):
        """
        Force a measurement, regardless of the auto-trigger. We recommend stopping the measurement before (stop_measuring())

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.SINGLE_MEASUREMENT)

    def restart_device(self):
        """
        Restart the device.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.RESTART)

    def request_status(self):
        """
        Request the device status.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.SEND_STATUS)

    def sleep(self):
        """
        Set the instrument into sleep mode to save power. Call wake_up before continuing to measure.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.SLEEP)

    def wake_up(self):
        """
        Wake up the instrument to resume measurements.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.WAKE_UP)

    def run_ota(self):
        """
        Run over-the-air firmware update of the instrument. Important - do not disturb the instrument for about
        5 minutes after, it will restart itself when the OTA is done and load the new firmware.

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.RUN_OTA)
        print("Started OTA. This will take about 5 min.")

    def restart(self):
        """
        Restarts the device - similar effect as powering it off and on

        :return: None
        """
        self._send_command(matoha_message_pb2.MatohaMessage.command_t.RESTART)

