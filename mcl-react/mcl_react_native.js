import React from 'react';
import {NativeEventEmitter, NativeModules, Platform } from 'react-native';
import BleManager from 'react-native-ble-manager';

const matohaMessage = require('../mcl/matoha_message/matoha_message_pb');
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

// Matoha BLE service and characteristic UUIDs
const MATOHA_SVC_UUID = '3C02B9F1-9BDF-4A69-92C4-33279FAE2FF9';
const MATOHA_MAIN_CH = "54B476DB-4A2E-4489-858C-BB0A98387DBC";

// Matoha BLE devices may advertise under any of the names below
const ALLOWED_NAMES = ["PLASTELL", "MATOHA", "FABRITELL", "MATOHABENCH", "MATOHAOEM", "MATOHAHAND"];


function log(msg){
    console.log("[BLEInterface] " + JSON.stringify(msg));
}

function logError(msg){
    console.error("[BLEInterface] " + JSON.stringify(msg));
}

export class MatohaBLEInterface {
    constructor(){
        this.started = false;
        this.devices = new Map();
        this.scanning = false;
        this.connected = false;
        this.disconnectListeners = {};
        this.scanDoneTrigger = null;
        this.measurementSubscriptionCallback = null;
        this.multipartBuffer = [];
        this.lastMeasurementId = -1;
        this.multipartPartsReceived = [];

        this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
        this.handleStopScan = this.handleStopScan.bind(this);
        this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(this);
        this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(this);
    }

    /**
     * Start the BLE interface
     *
     * This needs to be called before any other methods.
     */
    start() {
        if (Platform.OS === 'android') {
            BleManager.enableBluetooth()
                .then(() => {
                    // Success code
                    log('Bluetooth is enabled.');
                })
                .catch((error) => {
                    // Failure code
                    logError("The user refused to enable bluetooth");
                });
        }

        if(!this.started) {
            this.started = true;

            BleManager.start({showAlert: true});

            this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);
            this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);
            this.handlerDisconnect = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral);
            this.handlerUpdate = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic);
        } else {
            log("BLEInterface already started");
        }
    }


    componentWillUnmount() {
        this.handlerDiscover.remove();
        this.handlerStop.remove();
        this.handlerDisconnect.remove();
        this.handlerUpdate.remove();
    }

    /**
     * Callback when a device disconnects
     *
     * @param data
     */
    handleDisconnectedPeripheral(data) {
        log('Disconnected from ' + data.peripheral);
        Object.entries(this.disconnectListeners).forEach((listener) => {
            listener[1]();
        });
        this.disconnectListeners = {};
    }

    /**
     * Check if it is a multipart message.
     *
     * Matoha devices use multipart messages to get around the Bluetooth packet size limits. A multipart message
     * is indicated with a header with the first 4 bytes 0xAA 0xBB 0xCC 0xDD.
     *
     * @param data {Uint8Array} Raw data as received over Bluetooth
     * @returns {boolean}
     */
    checkMessageMultipart(data){
        // Header (10 bytes)
        //  4 x Multipart warning bytes 0xAA 0xBB 0xCC 0xDD
        //  Frame ID byte
        //  Total number of chunks byte
        //  Measurement counter byte
        //  Padding to 10 bytes

        if(data.length <= 10) { // the header is 10 bytes so the message must be larger
            return false;
        }
        return data[0] === 0xAA && data[1] === 0xBB && data[2] === 0xCC && data[3] === 0xDD;
    }

    /**
     * Handle a received value from a characteristic - this is typically a notification after a measurement.
     * @param data {Object} Data received from the BLEManager library
     * @returns {*}
     */
    handleUpdateValueForCharacteristic(data) {
        log('Received data (len = ' + data.value.length + ' ) from ' + data.peripheral + ' characteristic ' + data.characteristic + JSON.stringify(data.value));

        // Determine if a callback method has been registered otherwise there is no point in analyzing the data
        if(this.measurementSubscriptionCallback){

            // Longer messages are encoded as multipart messages
            if(this.checkMessageMultipart(data.value)){
                const partNo = data.value[4];
                const totalParts = data.value[5];
                const measurementId = data.value[6];
                log("Multipart message, part " + (partNo+1) + " out of " + totalParts + " meas no " + measurementId);

                // Is this a new measurement?
                if(measurementId !== this.lastMeasurementId){
                    this.multipartBuffer = data.value.slice(10);
                    this.lastMeasurementId = measurementId;
                    this.multipartPartsReceived = Array(totalParts).fill(false);
                    this.multipartPartsReceived[partNo] = true;
                } else {
                    if(this.multipartPartsReceived[partNo] === false) {
                        this.multipartBuffer.push(...data.value.slice(10));
                        this.multipartPartsReceived[partNo] = true;
                    }
                }

                log("Multipart status " + JSON.stringify(this.multipartPartsReceived));

                // Received in full?
                if(this.multipartPartsReceived.every(v => v === true)){
                    try {
                        log(JSON.stringify(this.multipartBuffer));
                        const deserialised = matohaMessage.MatohaMessage.deserializeBinary(this.multipartBuffer);
                        return this.measurementSubscriptionCallback(deserialised.toObject());
                    } catch (e) {
                        logError("Multipart measurement deserialise error", e);
                        return this.measurementSubscriptionCallback(null);
                    }
                }
            } else {
                try {
                    log("Decoding single-part message.");
                    let decodedMsg = matohaMessage.MatohaMessage.deserializeBinary(data.value).toObject();
                    return this.measurementSubscriptionCallback(decodedMsg);
                } catch (e) {
                    logError("Measurement deserialise error", e);
                    return this.measurementSubscriptionCallback(null);
                }
            }
        }
    }

    handleStopScan() {
        this.scanning = false;
        if(this.scanDoneTrigger) {
            this.scanDoneTrigger();
            this.scanDoneTrigger = null;
        }
    }

    /**
     * Start scanning for devices
     *
     * @param scanDoneTrigger Your function to be triggered when the scan is done
     * @param instrumentFoundTrigger Your function to be triggered when an instrument is found which will be called with the instrument object as a parameter
     */
    startScan(scanDoneTrigger, instrumentFoundTrigger) {
        log("BLE scan start requested");
        if (!this.scanning) {
            this.devices = new Map();
            this.scanning = true;
            this.scanDoneTrigger = scanDoneTrigger;
            this.instrumentFoundTrigger = instrumentFoundTrigger;

            // number of matches etc. settings from https://developer.android.com/reference/android/bluetooth/le/ScanSettings.html
            BleManager.scan([], 30, false, {numberOfMatches: 1, matchMode: 1, scanMode: 2}).then();
        }
    }

    /**
     * Stop scanning for devices
     */
    stopScan(){
        this.handleStopScan();
    }


    /**
     * Called on every device found during a scan.
     *
     * This will call the user-specified instrumentFoundTrigger function if the device is a recognised instrument.
     *
     * @param peripheral {Object} The device object from the BLEManager library
     */
    handleDiscoverPeripheral(peripheral) {
        if(peripheral.name === null || typeof peripheral.name !== 'string'){
            return;
        }
        let processedName = peripheral.name.toUpperCase().split("-")[0];

        if (ALLOWED_NAMES.includes(processedName) && !this.devices.has(peripheral.id)) {
            log('Got ble peripheral', peripheral);
            if(peripheral.rssi === 0){
                peripheral.name += " (paired)";
            }
            this.devices.set(peripheral.id, peripheral);
            if(this.instrumentFoundTrigger) {
                this.instrumentFoundTrigger();
            }
        }
    }

    /**
     * Connect to a device
     *
     * You need to start scanning first, then connect to a device based on a callback from handleDiscoverPeripheral
     *
     * @param uuid {string} UUID of the device to connect to
     */
    connect(uuid){
        BleManager.connect(uuid)
            .then(() => {
                log('Connected to a device.');
                this.connected = true;
                BleManager.retrieveServices(uuid)
                    .then((peripheralInfo) => {
                        log('Peripheral info:', peripheralInfo);
                    });
            })
            .catch((error) => {
                logError("Device connection error", error);
            });
    }

    /**
     * Disconnect from a device.
     *
     * @param uuid  {string} UUID of the device to disconnect from
     */
    disconnect(uuid){
        BleManager.disconnect(uuid);
    }

    /**
     * Check if the device is complaining about the MTU (Bluetooth packet size) being too small
     *
     * @param data {Array} Data received from the BLEManager library
     * @returns {boolean}
     */
    checkWrongMTUResponse(data){
        // It will return a 2-byte array [123, 0] if the MTU is too small
        return data.length === 2 && data[0] === 123;
    }

    /**
     * Parse the protobuf-encoded device status message
     *
     * @param data {Array} Data received from the BLEManager library
     *
     */
    deserialiseStatus(data){
        let incoming_status;
        try {
            incoming_status = matohaMessage.MatohaMessage.deserializeBinary(data).toObject();
        } catch (e) {
            let buff = new Uint8Array(data.length);
            for(let i=0; i < data.length; i++){
                buff[i] = data[i];
            }
            incoming_status = matohaMessage.MatohaMessage.deserializeBinary(buff).toObject();
        }
        log("Incoming status message from the device: " + JSON.stringify(incoming_status));
        return incoming_status;
    }

    /**
     * Read the device status.
     *
     * @param uuid of the device to read from
     * @returns {Promise<Object>}
     */
    async readStatus(uuid){
        const statusData = await BleManager.read(uuid, MATOHA_SVC_UUID, MATOHA_MAIN_CH);
        if(statusData !== null) {
            log("Read the following status from the device: " + JSON.stringify(statusData));

            // Check if the device is complaining about the MTU (Bluetooth packet size) being too small
            if(this.checkWrongMTUResponse(statusData)){
                log("MTU not (yet) correct.");
                return false;
            } else {
                return this.deserialiseStatus(statusData);
            }
        } else {
            log("Received an empty status message from the device.");
        }
    }

    /**
     * Send a command to the device.
     *
     * @param instrumentID {string} UUID of the device to send the command to
     * @param command {string} Command to send (single byte from the matohaMessage.MatohaMessage.Command enum)
     */
    sendCommand(instrumentID, command){
        let msg = new matohaMessage.MatohaMessage();
        msg.setCommand(command);
        let data = Array.from(msg.serializeBinary());

        log("Sending the command: " + command);
        BleManager.write(instrumentID, MATOHA_SVC_UUID, MATOHA_MAIN_CH, data, data.length)
            .then(() => {
                log("Command sent.");
            })
            .catch((error) => {
                logError("BLE command sending error", error);
            });
    }

    /**
     * Subscribe to device measurements.
     *
     * @param dev_id {string} UUID of the device to subscribe to
     * @param callback {function} Callback function to call when a measurement is received
     * @returns {Promise<boolean>} successful?
     */
    async subscribeMeasurement(dev_id, callback){
        try {
            await BleManager.startNotification(dev_id, MATOHA_SVC_UUID, MATOHA_MAIN_CH);
            this.measurementSubscriptionCallback = callback;
            log('Subscribed to measurements');
            return true;
        } catch(error){
            log("Measurement subscription error " + error);
            return false;
        }
    }

    /**
     * Request to stop receiving measurements from the device.
     *
     * @param dev_id {string} UUID of the device to unsubscribe from
     * @returns {Promise<boolean>} successful?
     */
    async unsubscribeMeasurement(dev_id){
        try {
            await BleManager.stopNotification(dev_id, MATOHA_SVC_UUID, MATOHA_MAIN_CH);
            this.measurementSubscriptionCallback = null;
            log('Unsubscribed from measurements');
            return true;
        } catch(error){
            log("Measurement unsubscription error " + error);
            return false;
        }
    }
}
