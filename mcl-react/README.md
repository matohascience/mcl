# MCL for React Native

An example of communicating with our device using React Native and Bluetooth.

This is a work in progress and needs to be refactored and packaged as a library.

## Dependencies

This relies on the following libraries:
- react-native-ble-manager https://github.com/innoveit/react-native-ble-manager
- React & React Native
- Compiled version of Matoha Message (currently in mcl/matoha_message/matoha_message_pb.js)
